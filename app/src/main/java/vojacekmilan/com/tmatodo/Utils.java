package vojacekmilan.com.tmatodo;

import android.content.ContentProviderClient;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by milan on 26.10.16.
 */

public class Utils {
    public static void error(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void error(Context context) {
        error(context, "napicu");
    }
}
