package vojacekmilan.com.tmatodo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import vojacekmilan.com.tmatodo.listener.ListViewListener;


public class MainActivity extends AppCompatActivity implements Constants {

    private ArrayAdapter<TodoItem> adapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setProgressBar();

        setSupportActionBar(toolbar);

        setAdapter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (intent == null) {
            return;
        }

        String title = intent.getStringExtra(EditActivity.TITLE);
        String content = intent.getStringExtra(EditActivity.CONTENT);

        if (title == null || content == null) {
            return;
        }

        boolean isDone = intent.getBooleanExtra(EditActivity.IS_DONE, false);
        int id = intent.getIntExtra(EditActivity.ID, TodoItem.INSERT_STATE);

        TodoItem item = new TodoItem(id, title, content, isDone);
        MyDbOpenHelper helper = MyDbOpenHelper.getInstance(this);
        helper.store(item);
        loadTasks();

        super.onActivityResult(requestCode, resultCode, intent);
    }

    public ArrayAdapter<TodoItem> getAdapter() {
        return adapter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.menu_add:
                startEditActivityForInsert();
                return true;
            case R.id.menu_download:
                DownloadTasksTask task = new DownloadTasksTask(this);
                task.execute();
                return true;
            case R.id.menu_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        loadTasks();
        super.onResume();
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void loadTasks() {
        SharedPreferences preferences = getSharedPreferences(SettingsActivity.SETTINGS, MODE_PRIVATE);

        MyDbOpenHelper instance = MyDbOpenHelper.getInstance(this);
        boolean showDoneItems = preferences.getBoolean(SettingsActivity.SHOW_DONE_TASKS, false);
        adapter.clear();
        adapter.addAll(instance.fetchItems(showDoneItems));
        adapter.notifyDataSetChanged();
    }

    private void setAdapter() {
        adapter = new ArrayAdapter<>(this, R.layout.item_todo);
        ListView listView = (ListView) findViewById(R.id.listview_items);
        ListViewListener listener = new ListViewListener(this);
        listView.setOnItemClickListener(listener);
        listView.setAdapter(adapter);

        loadTasks();
    }

    private void startEditActivityForInsert() {
        Intent intent = new Intent(this, EditActivity.class);
        startActivityForResult(intent, 0);
    }

    private void setProgressBar() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar_download);
        progressBar.setIndeterminate(true);
        hideProgressBar();
    }
}
