package vojacekmilan.com.tmatodo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class EditActivity extends AppCompatActivity implements Constants, View.OnKeyListener {

    public static final String CONTENT = "content";
    public static final String TITLE = "title";
    public static final String IS_DONE = "is_done";
    public static final String ID = "id";

    private EditText title;
    private EditText content;

    private CheckBox isDone;

    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        init();

        fillInputs();
    }

    private void fillInputs() {
        Intent intent = getIntent();

        id = intent.getIntExtra(ID, TodoItem.INSERT_STATE);
        String title = intent.getStringExtra(TITLE);
        String content = intent.getStringExtra(CONTENT);
        boolean isDone = intent.getBooleanExtra(IS_DONE, false);

        this.title.setText(title);
        this.content.setText(content);
        this.isDone.setChecked(isDone);
    }

    private void init() {
        title = (EditText) findViewById(R.id.edit_title);
        content = (EditText) findViewById(R.id.edit_content);
        isDone = (CheckBox) findViewById(R.id.checkbox_is_done);

        content.setOnKeyListener(this);
    }

    private void returnResult() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(CONTENT, content.getText().toString());
        intent.putExtra(TITLE, title.getText().toString());
        intent.putExtra(ID, id);
        intent.putExtra(IS_DONE, isDone.isChecked());
        setResult(STATUS_OK, intent);
        finish();
    }

    /**
     * this implements listener for content edit text
     *
     * @param view
     * @param i
     * @param keyEvent
     * @return
     */
    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            returnResult();
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_ok) {
            returnResult();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
