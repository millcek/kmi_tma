package vojacekmilan.com.tmatodo.listener;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import vojacekmilan.com.tmatodo.EditActivity;
import vojacekmilan.com.tmatodo.MainActivity;
import vojacekmilan.com.tmatodo.TodoItem;
import vojacekmilan.com.tmatodo.Utils;

/**
 * Created by milan on 26.10.16.
 */

public class ListViewListener implements ListView.OnItemClickListener {

    private MainActivity mainActivity;

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        ArrayAdapter<TodoItem> adapter = mainActivity.getAdapter();
        Intent intent = new Intent(mainActivity, EditActivity.class);

        if (adapter != null && adapter.getItem(i) != null) {
            TodoItem item = adapter.getItem(i);
            if (item != null) {
                intent.putExtra(EditActivity.TITLE, item.getTitle());
                intent.putExtra(EditActivity.CONTENT, item.getContent());
                intent.putExtra(EditActivity.IS_DONE, item.isDone());
                intent.putExtra(EditActivity.ID, item.getId());

                mainActivity.startActivityForResult(intent, 0);
            }
        } else {
            Utils.error(mainActivity);
        }
    }

    public ListViewListener(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }
}
