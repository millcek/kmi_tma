package vojacekmilan.com.tmatodo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by milan on 2.11.16.
 */

public class MyDbOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "todo_db";
    private static final String TODO_TABLE = "todo";
    private static final int VERSION = 1;

    private static MyDbOpenHelper helper = null;

    public static final String ID = "rowid";
    public static final String CONTENT = "content";
    public static final String TITLE = "title";
    public static final String IS_DONE = "is_done";

    private MyDbOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    public static MyDbOpenHelper getInstance(Context context) {
        if (helper == null) {
            helper = new MyDbOpenHelper(context);
        }
        return helper;
    }

    public List<TodoItem> fetchItems(boolean withDoneItems) {
        SQLiteDatabase readableDatabase = this.getReadableDatabase();
        String doneSelection = withDoneItems ? null : IS_DONE + " = 0";

        Cursor cursor = readableDatabase.query(TODO_TABLE,
                new String[]{ID, CONTENT, TITLE, IS_DONE},
                doneSelection,
                null,
                null,
                null,
                null
        );

        List<TodoItem> out = getTodoItems(cursor);
        cursor.close();
        return out;
    }

    @NonNull
    private List<TodoItem> getTodoItems(Cursor cursor) {
        List<TodoItem> out = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ID));

                String title = cursor.getString(cursor.getColumnIndex(TITLE));
                String content = cursor.getString(cursor.getColumnIndex(CONTENT));

                boolean isDone = cursor.getInt(cursor.getColumnIndex(IS_DONE)) == 1;

                out.add(new TodoItem(id, title, content, isDone));
            } while (cursor.moveToNext());
        }
        return out;
    }

    public void store(TodoItem item) {
        SQLiteDatabase writableDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TITLE, item.getTitle());
        values.put(CONTENT, item.getContent());
        values.put(IS_DONE, item.isDone() ? 1 : 0);

        if (item.id == TodoItem.INSERT_STATE) {
            writableDatabase.insert(TODO_TABLE, null, values);
        } else {
            writableDatabase.update(TODO_TABLE, values, ID + "=" + item.id, null);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE %s (%s TEXT, %s TEXT, %s INTEGER)",
                TODO_TABLE, TITLE, CONTENT, IS_DONE));
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
