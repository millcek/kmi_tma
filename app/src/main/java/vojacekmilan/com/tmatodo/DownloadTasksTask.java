package vojacekmilan.com.tmatodo;

import java.io.IOException;
import java.net.URL;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by milan on 9.11.16.
 */

public class DownloadTasksTask extends AsyncTask<Object, Object, Boolean> {

    private final static String URL = "http://www.novaklukas.cz/tma/seminars/7/tma_todo.json";

    private MainActivity activity;
    private int errorMessage;

    public DownloadTasksTask(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        activity.showProgressBar();
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Object[] objects) {
        String json;

        try {
            json = getJsonStringFromWeb();
        } catch (IOException e) {
            e.printStackTrace();
            errorMessage = R.string.connection_error;
            return false;
        }

        List<TodoItem> tasksFromJsonString;
        try {
            tasksFromJsonString = getTasksFromJsonString(json);
        } catch (JSONException e) {
            e.printStackTrace();
            errorMessage = R.string.server_response_error;
            return false;
        }

        MyDbOpenHelper instance = MyDbOpenHelper.getInstance(activity);

        for (TodoItem item : tasksFromJsonString) {
            instance.store(item);
        }

        instance.close();
        return true;
    }

    @Override
    protected void onPostExecute(Boolean successful) {
        activity.hideProgressBar();
        if (!successful) {
            Log.e(this.getClass().getName(), activity.getString(errorMessage));
            return;
        }
        activity.loadTasks();
    }

    public static List<TodoItem> getTasksFromJsonString(String source) throws JSONException {

        List<TodoItem> out = new ArrayList<>();
        JSONArray ja = new JSONArray(source);

        for (int i = 0; i < ja.length(); i++) {
            JSONObject jo = ja.getJSONObject(i);
            String name = jo.getString("title");
            String content = jo.getString("content");
            boolean isDone = jo.getBoolean("done");
            out.add(new TodoItem(name, content, isDone));
        }

        return out;
    }

    private String getJsonStringFromWeb() throws IOException {
//        return "[ { \"title\": \"Úkol č.1\", \"content\": \"Splnit úkol č.1\", \"done\": true }, { \"title\": \"Úkol č.2\", \"content\": \"Splnit úkol č.2\", \"done\": true }, { \"title\": \"Úkol č.3\", \"content\": \"Splnit úkol č.3\", \"done\": true }, { \"title\": \"Úkol č.4\", \"content\": \"Splnit úkol č.4\", \"done\": true }, { \"title\": \"Úkol č.5\", \"content\": \"Splnit úkol č.5\", \"done\": true }, { \"title\": \"Úkol č.6\", \"content\": \"Splnit úkol č.6\", \"done\": true }, { \"title\": \"Úkol č.7\", \"content\": \"Splnit úkol č.7\", \"done\": false }, { \"title\": \"Úkol č.8\", \"content\": \"Splnit úkol č.8\", \"done\": false }, { \"title\": \"Úkol č.9\", \"content\": \"Splnit úkol č.9\", \"done\": false }, { \"title\": \"Úkol č.10\", \"content\": \"Splnit úkol č.10\", \"done\": false }, { \"title\": \"Úkol č.11\", \"content\": \"Splnit úkol č.11\", \"done\": false }, { \"title\": \"Úkol č.12\", \"content\": \"Splnit úkol č.12\", \"done\": false }, { \"title\": \"Vymyslet závěrečný projekt\", \"content\": \"Popsat vyučujícímu téma závěrečného projektu\", \"done\": false }, { \"title\": \"Odprezentovat závěrečný projekt\", \"content\": \"V pěti minutách představit závěrečný projekt na posledním semináři\", \"done\": false } ]\n";
        HttpURLConnection connection = (HttpURLConnection) (new URL(URL).openConnection());
        Scanner sc = new Scanner(connection.getInputStream());
        StringBuilder builder = new StringBuilder();
        while (sc.hasNextLine()) {
            builder.append(sc.nextLine());
        }
        return builder.toString();
    }
}
