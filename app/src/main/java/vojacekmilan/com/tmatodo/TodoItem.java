package vojacekmilan.com.tmatodo;

/**
 * Created by milan on 26.10.16.
 */

public class TodoItem {
    public static final int INSERT_STATE = -1;

    protected int id;

    protected String title;
    protected String content;

    protected boolean isDone = false;

    public TodoItem(String title, String content) {
        this.id = INSERT_STATE;
        this.title = title;
        this.content = content;
    }

    public TodoItem(String title, String content, boolean isDone) {
        this.id = INSERT_STATE;
        this.title = title;
        this.content = content;
        this.isDone = isDone;
    }

    public TodoItem(int id, String title, String content) {
        this.id = id;
        this.title = title;
        this.content = content;
    }


    public TodoItem(int id, String title, String content, boolean isDone) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.isDone = isDone;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TodoItem todoItem = (TodoItem) o;

        if (isDone != todoItem.isDone) return false;
        if (title != null ? !title.equals(todoItem.title) : todoItem.title != null) return false;
        return content != null ? content.equals(todoItem.content) : todoItem.content == null;

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (isDone ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return title;
    }
}
