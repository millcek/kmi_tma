package vojacekmilan.com.tmatodo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.CompoundButton;
import android.widget.Switch;

public class SettingsActivity extends AppCompatActivity {

    public static final String SETTINGS = "settings";
    public static final String SHOW_DONE_TASKS = "show_done_tasks";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        setDoneTasksCheckbox();
    }

    private void setDoneTasksCheckbox() {
        final Switch checkBox = (Switch) findViewById(R.id.switch_show_done_tasks);
        SharedPreferences settings = getSharedPreferences(SETTINGS, MODE_PRIVATE);
        checkBox.setChecked(settings.getBoolean(SHOW_DONE_TASKS, false));

        checkBox.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferences settings = getSharedPreferences(SETTINGS, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(SHOW_DONE_TASKS, checkBox.isChecked());
                editor.apply();
            }
        });
    }
}
