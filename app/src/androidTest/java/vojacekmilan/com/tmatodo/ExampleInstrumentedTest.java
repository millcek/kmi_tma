package vojacekmilan.com.tmatodo;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void testInsertAndFetch() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        TodoItem item = new TodoItem("ahoj", "světe");

        MyDbOpenHelper helper = MyDbOpenHelper.getInstance(appContext);
        helper.store(item);

        List<TodoItem> todoItems = helper.fetchItems(true);
        TodoItem fetchedItem = todoItems.get(todoItems.size() - 1);

        assertEquals(item, fetchedItem);
    }

    @Test
    public void testFetch() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        MyDbOpenHelper helper = MyDbOpenHelper.getInstance(appContext);

        List<TodoItem> todoItems = helper.fetchItems(true);

        Log.i("ahoj", "ahoj tyvlaksjdflkja");
        for (TodoItem item :
                todoItems) {
            System.out.println(item.isDone());
        }
    }
}
